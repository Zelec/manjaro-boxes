# manjaro-boxes
### Forked from [arch-boxes](https://gitlab.archlinux.org/archlinux/arch-boxes)
[![CI Status](https://gitlab.com/zelec/manjaro-boxes/badges/master/pipeline.svg)](https://gitlab.com/zelec/manjaro-boxes/-/pipelines)

- [**Vagrant Cloud**](https://app.vagrantup.com/Zelec/boxes/manjarolinux)
- [**Download latest artifacts**](https://minio-remote.tgdev.ca/minio/public/manjaro-boxes/)

Manjaro-boxes provides automated builds of Manjaro Linux for different providers and formats.

## Images

### Vagrant
If you're a vagrant user, you can just go to the [** Vagrant Cloud page**](https://app.vagrantup.com/Zelec/boxes/manjarolinux) and follow the instructions there.

### Cloud image
If you want to run Manjaro Linux in the cloud, you can use the cloud-image, which is preconfigured to work in most cloud environments. It is built daily and can be downloaded [here](https://minio-remote.tgdev.ca/minio/public/manjaro-boxes/) (`Manjaro-Linux-x86_64-cloudimg-xxxxxxxx.xxxx.qcow2`).

#### Basic image
The basic image is meant for local usage and comes preconfigured with the user `arch` (pw: `arch`) and sshd running.

If you are running the cloud-image with QEMU, it can in some cases\* be beneficial to run the [QEMU guest-agent](https://wiki.qemu.org/Features/GuestAgent). This can be done with the following user-data:
```yaml
#cloud-config
packages:
  - qemu-guest-agent
runcmd:
  - [ systemctl, daemon-reload ]
  - [ systemctl, enable, qemu-guest-agent ]
  - [ systemctl, start, qemu-guest-agent ]
```
*\*ex: when using [Proxmox](https://pve.proxmox.com/wiki/Qemu-guest-agent) or [oVirt](https://www.ovirt.org/develop/internal/guest-agent/understanding-guest-agents-and-other-tools.html). Please be aware, that the agent basically gives the host root access to the guest.*

Be advised, however, that the automatic builds are cleaned up after a few days so you can't hard-code a specific image version anywhere.

## Development

### Dependencies
You'll need the following dependencies:

* qemu
* libisoburn

### How to build this
The official builds are done in GitLab CI and uploaded to a Minio S3 backend for artifact storage.
The images can be built locally by running:

    ./build-host.sh
